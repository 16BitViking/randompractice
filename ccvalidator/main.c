#include <stdio.h>
#include <string.h>

#define ERR_ARGS -1
#define ERR_WHAT -2
#define ERR_NaN -3
#define TRUE 1
#define FALSE 0

void print_usage();
int validate(char * );
int doublesum(int);
int ctoi(char);

int main(int argc, char * argv[])
{
	int valid = FALSE;

	if(argc < 2)
	{
		print_usage();
		return ERR_ARGS;
	}
	else
	{
		valid = validate(argv[1]);
		if (valid == TRUE)
		{
			printf("VALID\n");
		}
		else
		{
			printf("INVALID\n");
		}

		return 0;
	}


	return ERR_WHAT; /* how did you get here */
}

/* tells you how to use the damn thing */
void print_usage()
{
	printf("Usage: ccvalidator <ccnumber>\n");
}

/* converts single char to int, -1 if NaN */
int ctoi(char c)
{
	if(c < '0' || c > '9')
	{
		return ERR_NaN;
	}
	else
	{	
		int num = c - '0';
		return num;
	}
}

/* only works for x < 10, doubles x and sums the digits (for example, 17 = 8, 16 = 7, ...) */
int doublesum(int x)
{
	if(x < 5) return x*2;
	else
	{
		return x*2-9;
	}		
}

int validate(char * num)
{
	int length, index, sum;

	length = strlen(num);
	sum = 0;

	/* From the rightmost digit, which is the check digit, moving left, double the value of every second digit; if the product of this doubling operation is greater than 9 (e.g., 8 × 2 = 16), then sum the digits of the products (e.g., 16: 1 + 6 = 7, 18: 1 + 8 = 9). */

	for(index = length-1; index >= 0; index--)
	{
		if(ctoi(num[index]) == ERR_NaN) return FALSE;
		if((length - index) % 2 == 0)
		{
			sum += doublesum(ctoi(num[index]));
		}
		else
		{	
			sum += ctoi(num[index]);
		}
	}


	/* Take the sum of all the digits. */
	/* If the total modulo 10 is equal to 0 (if the total ends in zero) then the number is valid according to the Luhn formula; else it is not valid. */

	/* printf("%d\n", sum); */

	if(sum % 10 == 0)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
	
}
