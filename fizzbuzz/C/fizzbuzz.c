#include <stdio.h>

/* omg fizzbuzz I'm so excited */

int main(int argc, char * argv[])
{
	int i;

	for(i = 1; i <= 100; i++)
	{
		int buzzin = 0;

		if(i % 3 == 0)
		{
			printf("Fizz");
			buzzin++;
		}
		if(i % 5 == 0)
		{
			printf("Buzz");
			buzzin++;
		}
		if(!buzzin)
		{
			printf("%d",i);
		}

		printf("\n");
	}

	return 0;	
}
